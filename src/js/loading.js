
import { imgArray } from './portfolio-rotate.js';

const img_Speed = 8;
var counter = 0;
// var timer = 300;

//TODO: 測試用函數, 記得刪除
export function printText() {
  console.log('flag1');
}

function loaderRotate() {
  if (counter > imgArray.length - 1) {
    counter = 0;
  }
  document.getElementById('loader').src = imgArray[counter++].src;

  // setTimeout(imgRotate, timer);
  // window.requestAnimationFrame(imgRotate);
  // use requestAnimationFrame instead of settimeout to get the smooth animation
}
//inspired by Web Dev Simplified "How To Code The Snake Game In Javascript"

let lastRenderedTime = 0;
let stopped = true;
let requestId = 0;

function timer(currentTime) {
  if (stopped == false) {
    requestId = window.requestAnimationFrame(timer);
    const secondsSinceLastRendered = (currentTime - lastRenderedTime) / 1000; //to milliseconds //0.00....
    if (secondsSinceLastRendered < 1 / img_Speed) {
      return;
    } //if img_Speed = 2 then start caculate larger than 0.5 seconds
    lastRenderedTime = currentTime; //first update the lastRenderedTime
    loaderRotate();
  }
}
// window.onload = requestAnimationFrame(timer);
// imgRotate();

 function  startAnimation() {
  // http();
  stopped = false;
  requestAnimationFrame(timer);
  loaderRotate();
}

 function stopAnimation() {
  console.log('FLAG 1: loading.stopAnimation');
  if (requestId) {
    window.cancelAnimationFrame(requestId);
    stopped = true;
  }
}

 startAnimation();

$(window).on('load', function () {
  setTimeout(removeLoader, 1000);
  // removeLoader() //wait for page load PLUS two seconds.
});

 function removeLoader() {
  $('.loading').fadeOut(500, function () {
    // fadeOut complete. Remove the loading div
    $('.loading').remove();
    //makes page more lightweight
     stopAnimation();
  });
}

function reqOnload (res) {
  console.log('FLAG http success:', res);
}
function reqError (err) {
  console.log('http 錯誤', err)
}

function http() {
  var request = new XMLHttpRequest(); 
// 定義連線方式
request.open('get', 'http://localhost:2020/', true);
// 送出請求
request.send();
// 如果成功就執行 reqOnload()
request.onload = reqOnload; 
// 失敗就 reqError()
request.onerror = reqError;
}

http()