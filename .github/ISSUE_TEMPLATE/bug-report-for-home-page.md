---
name: Bug report for home page
about: for both front and back end
title: 'portfolio-bug '
labels: ''
assignees: ''

---

##  前端問題整理
1.　我有 function module 的 import export 問題，在** .scr/js/portfolio-rotate.js** 和 **.scr/js/loading.js**兩個檔案裡，分開也能用但我想做module 
2.　如果用你上次說的Loading start/Loading end會跟我的開場動畫衝突，因為有些開場動畫不方便延遲3.    目前 loading page跑照片有點太慢，我想用node js改良，變成Loading page直接加loading百分比，但是我code寫好了不知到要放哪
3.　如果直接用node_modules裡的font-awesome會找不到檔，但是用CDN找的到
4.　想用vi改npm rc，不知道怎麼加file


## for example, I have problem of export these codes from portfolio-rotate.js to loading.js
```javascript
function stopAnimation() {
    if (requestId) {
        window.cancelAnimationFrame(requestId);
        stopped = true;
    }
}
```
######  我還有一些後端問題，就不上傳了直接demo
