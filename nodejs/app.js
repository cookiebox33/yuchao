const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors())
app.get('/', (req, res) => {
    res.json({
        message: 'Hello World'
    });
});

app.get('/:name', (req, res) => {
    let name = req.params.name;

    res.json({
        message: `Hello ${name}`
    });
});

app.listen(2020, () => {
    console.log('server is listening on port 2020');
});
// const http = require('http');

// const hostname = '127.0.0.1';
// const port = 4000;

// const server = http.createServer((req, res) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.statusCode = 200;
//   res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World');
// });

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });
